// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#pragma once

#include "GameFramework/PlayerController.h"
#include "ItemSystem/Interactable.h"
#include "ItemSystem/InventoryComponent.h"
#include "NevLDGameInstance.h"
#include "NevLDPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class NEVLD_API ANevLDPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:

	virtual void SetupInputComponent() override;

	virtual void BeginPlay() override;
	
	/** Interact action when the proper input is given */
	void Interact();

public:

	ANevLDPlayerController();

	/** The interactable currently visible by the player */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class AInteractable* InSightInteractable;

	/** The InventoryComponent C++ Class or Blueprint to use.
	* 
	* The actual component is instantiated and attached when the game begins.
	* This is a necessary tradeoff because UE4 doesn't give the possibility to
	* directly select subclasses of a component in the editor and we want to
	* retain the flexibility of inheritance.
	* Init parameters of an InventoryComponent, though, have to be defined
	* directly in its C++ Constructor or Blueprint depending on the implementation.
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory Management")
	TSubclassOf<class UInventoryComponent> InventoryClass;

	/** The player's inventory component */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Inventory Management")
	UInventoryComponent* InventoryComponent;

	/** Open/Closes the Inventory GUI. Must be implemented using Blueprints. */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Inventory Management")
	void ToggleInventory();

};
