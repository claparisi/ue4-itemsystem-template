// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#include "NevLD.h"
#include "Interactable.h"


// Sets default values
AInteractable::AInteractable()
{
	Name = "Interactable Name";
	Hint = "Interact";
	InteractableMesh = CreateDefaultSubobject<UStaticMeshComponent>("InteractableMesh");
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();
}

void AInteractable::Interact_Implementation(ANevLDPlayerController* Controller)
{
	UE_LOG(LogTemp, Warning, TEXT("Unexpected interaction from Interactable base class, you should implement this through inheritance."));
}