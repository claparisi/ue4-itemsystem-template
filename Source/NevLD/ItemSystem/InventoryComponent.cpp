// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#include "NevLD.h"
#include "NevLDGameInstance.h"
#include "InventoryComponent.h"


// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	// Default Inventory values
	InventoryCapacity = 16;
	bSpawnOnDrop = false;
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
	
	if(InventoryCapacity > 0)
		Inventory.Reserve(InventoryCapacity);
}

uint8 UInventoryComponent::StoreItem(FName ItemID, uint8 StackSize)
{
	if (StackSize == 0)
		return 0;
	UNevLDGameInstance* GameInstance = Cast<UNevLDGameInstance>(GetWorld()->GetGameInstance());
	if (GameInstance != nullptr) {
		FStorableRow* Storable = GameInstance->GetStorableFromDataTable(ItemID);
		if (Storable != nullptr) {
			FInventorySlot Temp(*Storable, StackSize);
			//If it is a full stack or we have no other stack, just add the item in the new slot
			if (StackSize == Storable->MaxStackSize || Inventory.Find(Temp) == INDEX_NONE)
				return AddNewSlot(Temp);
			//Start by adding the StackSize to the other available stacks if possible
			//Linear search is required
			for (auto& Slot : Inventory) {
				if (Slot == Temp && Slot.StackSize < Slot.Item.MaxStackSize) {
					uint8 Diff = Slot.Item.MaxStackSize - Slot.StackSize;
					if (Diff >= Temp.StackSize) { //The stack to add is depleted
						Slot.StackSize += Temp.StackSize;
						ReloadInventory();
						return StackSize;
					}
					Temp.StackSize -= Diff;
					Slot.StackSize = Slot.Item.MaxStackSize;
				}
			}
			//We still have Temp.StackSize items to add in a new slot
			if (AddNewSlot(Temp) == 0) //We had no empty slots in the inventory
				return StackSize - Temp.StackSize;
			return StackSize;
		}
	}
	return 0;
}

uint8 UInventoryComponent::DropItem(FName ItemID, uint8 StackSize)
{
	if (StackSize == 0)
		return 0;
	if (bSpawnOnDrop) {
		AController* Controller = Cast<AController>(GetOwner());
		if (Controller == nullptr) {
			UE_LOG(LogTemp, Error, TEXT("DropItem: Unable to get this InventoryComponent owner"));
			return 0;
		}
		APawn* Pawn = Controller->GetPawn();
		if (Pawn == nullptr) {
			UE_LOG(LogTemp, Error, TEXT("DropItem: Unable to get the Pawn controlled by this InventoryComponent owner"));
			return 0;
		}
		UNevLDGameInstance* GameInstance = Cast<UNevLDGameInstance>(GetWorld()->GetGameInstance());
		if (GameInstance == nullptr) {
			UE_LOG(LogTemp, Error, TEXT("DropItem: Unable to get the GameInstance"));
			return 0;
		}
		FStorableRow* StorableRow = GameInstance->GetStorableFromDataTable(ItemID);
		if (StorableRow == nullptr) {
			UE_LOG(LogTemp, Error, TEXT("DropItem: Item [%s] not found in the Storable datatable"), *ItemID.ToString());
			return 0;
		}
		FVector Location = Pawn->GetActorLocation();
		Location.X += 32.0f;
		FRotator Rotation(0.0f, 0.0f, 0.0f);
		TSubclassOf<AStorable> StorableClass = StorableRow->Storable;
		if (StorableClass == NULL) {
			UE_LOG(LogTemp, Error, TEXT("DropItem: No StorableClass has been defined for Item [%s]"), *ItemID.ToString());
			return 0;
		}
		AStorable* Spawn = GetWorld()->SpawnActor<AStorable>(StorableClass, Location, Rotation);
		if (Spawn == nullptr) {
			UE_LOG(LogTemp, Error, TEXT("DropItem: Unable to Spawn Item [%s] in the game world"), *ItemID.ToString());
			return 0;
		}
	}
	return RemoveItem(ItemID, StackSize);
}

uint8 UInventoryComponent::RemoveItem(FName ItemID, uint8 StackSize)
{
	if (StackSize == 0)
		return 0;
	UNevLDGameInstance* GameInstance = Cast<UNevLDGameInstance>(GetWorld()->GetGameInstance());
	if (GameInstance != nullptr) {
		FStorableRow* Storable = GameInstance->GetStorableFromDataTable(ItemID);
		if (Storable != nullptr) {
			FInventorySlot Temp(*Storable, StackSize);
			//Start from the last, if insufficient StackSize, find with full backward scan
			int32 slotIdx = -1;
			if (!Inventory.FindLast(Temp, slotIdx))
				return 0;
			if (StackSize <= Inventory[slotIdx].StackSize)
				return RemoveItemAt((uint8)slotIdx, StackSize);

			//We weren't lucky. Linear search is required.
			for (int32 i = Inventory.Num() -1; i >= 0; i--) {
				if (Inventory[i] == Temp && StackSize <= Inventory[i].StackSize) 
					return RemoveItemAt((uint8)slotIdx, StackSize);
			}
		}
	}
	return 0;
}

uint8 UInventoryComponent::RemoveItemAt(uint8 SlotIdx, uint8 StackSize)
{
	if (StackSize == 0 || SlotIdx >= Inventory.Num())
		return 0;
	FInventorySlot* Slot = &Inventory[SlotIdx];
	if (Slot->StackSize > StackSize) {
		Slot->StackSize -= StackSize;
		ReloadInventory();
		return StackSize;
	}
	if (Slot->StackSize == StackSize) {
		Inventory.RemoveAt(SlotIdx, 1, !(InventoryCapacity > 0));//Do not shrink the TArray if the inventory has a finite, reserved, size
		ReloadInventory();
		return StackSize;
	}
	return 0;
}

bool UInventoryComponent::SplitStackAt(uint8 SlotIdx, uint8 StackSize)
{
	if (StackSize == 0 || SlotIdx >= Inventory.Num())
		return false;
	FInventorySlot* Slot = &Inventory[SlotIdx];
	if (Slot->StackSize <= StackSize) 
		return false;
	Slot->StackSize -= StackSize;
	UNevLDGameInstance* GameInstance = Cast<UNevLDGameInstance>(GetWorld()->GetGameInstance());
	if (GameInstance != nullptr) {
		FStorableRow* Storable = GameInstance->GetStorableFromDataTable(Slot->Item.ItemID);
		if (Storable != nullptr) {
			FInventorySlot Temp(*Storable, StackSize);
			uint8 addOutcome = AddNewSlot(Temp);
			if (addOutcome == StackSize) {
				ReloadInventory();
				return true;
			}
		}
	}
	Slot->StackSize += StackSize;
	return false;
}

uint8 UInventoryComponent::AddNewSlot(FInventorySlot &Slot) {
	if (InventoryCapacity < 1 || Inventory.Num() < InventoryCapacity) {
		Inventory.Add(Slot);
		ReloadInventory();
		return Slot.StackSize;
	}
	return 0;
}