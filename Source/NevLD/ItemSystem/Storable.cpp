// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#include "NevLD.h"
#include "NevLDPlayerController.h"
#include "ItemSystem/InventoryComponent.h"
#include "Storable.h"


AStorable::AStorable() {
	ItemID = FName("Storable ID");
	Hint = "Pick up";
	StackSize = 1;
}

void AStorable::Use_Implementation(ANevLDPlayerController* Controller, bool bRemoveFromInventory)
{
	//We don't want to remove the Item from the inventory if it has unlimited use charges...
	if (bRemoveFromInventory && Controller != nullptr && Controller->InventoryComponent != nullptr) {
		UInventoryComponent* Inventory = Cast<UInventoryComponent>(Controller->InventoryComponent);
		uint8 Removed = Inventory->RemoveItem(ItemID);
		if(Removed == 0)
			UE_LOG(LogTemp, Error, TEXT("Unable to remove this Storable [%s] from the Inventory Component"), *ItemID.ToString());
	}
	//...but upon use we always destroy this particular instance
	Destroy();
	//UE_LOG(LogTemp, Warning, TEXT("Unexpected Use of a Storable from base class, you should implement this through inheritance."));
}

void AStorable::Interact_Implementation(ANevLDPlayerController* Controller)
{
	//Once we are sure the Controller has stored this Storable, we remove it from the game world (destroy)
	if (Controller != nullptr && Controller->InventoryComponent != nullptr) {
		UInventoryComponent* Inventory = Cast<UInventoryComponent>(Controller->InventoryComponent);
		uint8 Stored = Inventory->StoreItem(ItemID, StackSize);
		if (Stored == StackSize)
			Destroy();
		else
			StackSize -= Stored;
	}
		
}
