// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#pragma once

#include "Components/ActorComponent.h"
#include "Storable.h"
#include "InventoryComponent.generated.h"

USTRUCT(BlueprintType)
struct FInventorySlot {
	GENERATED_BODY()

public:

	FInventorySlot() {
		StackSize = 0;
	}

	FInventorySlot(FStorableRow InItem, uint8 InStackSize) {
		Item = InItem;
		StackSize = InStackSize;
	}

	/** The Item stored in this Inventory Slot */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FStorableRow Item;

	/** The amount of Items stacked together in this Inventory Slot */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	uint8 StackSize;

	/** The equality among two slots is met when the two slots contain the same item. */
	bool operator==(const FInventorySlot Slot) const {
		return Item == Slot.Item;
	}

};

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class NEVLD_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	/** The capacity of the player's inventory (i.e. maximum amount of inventory slots) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory Variables")
	uint8 InventoryCapacity;

	/** The capacity of the player's inventory (i.e. maximum amount of inventory slots) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory Variables")
	bool bSpawnOnDrop;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Inventory Variables")
	TArray<FInventorySlot> Inventory;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public: 
	
	/** Store an Item in this InventoryComponent.
	*
	* @param ItemID The ID of the Item to store
	* @param StackSize The amount to store. Default = 1
	* @return The amount actually stored, can be 0 (e.g. Inventory full)
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory Management")
	uint8 StoreItem(FName ItemID, uint8 StackSize = 1);

	/** Drops an Item from this InventoryComponent.
	*
	* If bSpawnOnDrop == true and the passed StackSize > 1, a Storable instance 
	* will be placed in the game world having its stack size = StackSize.
	*
	* @param ItemID The ID of the Item to drop
	* @param StackSize The amount to drop. Default = 1.
	* @return The amount actually dropped, can be 0 (e.g. Item not present, input error, etc)
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory Management")
	uint8 DropItem(FName ItemID, uint8 StackSize = 1);

	/** Remove an Item from this InventoryComponent from the last stack that has at least StackSize amount.
	*
	* The current implementation is unable to remove items from multiple stacks
	* if the StackSize is greater than the size of every stored stack but lesser than the sum of them.
	*
	* @param SlotIdx The index of the slot from which the Item should be removed
	* @param StackSize The amount to remove. Default = 1
	* @return The amount actually removed, can be 0 (e.g. Item not present, input error, etc)
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory Management")
	uint8 RemoveItem(FName ItemID, uint8 StackSize = 1);

	/** Remove an Item from this InventoryComponent according to a specific slot.
	*
	* @param SlotIdx The index of the slot from which the Item should be removed
	* @param StackSize The amount to remove. Default = 1
	* @return The amount actually removed, can be 0 (e.g. Input error)
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory Management")
	uint8 RemoveItemAt(uint8 SlotIdx, uint8 StackSize = 1);

	/** Split an Item stack in two.
	*
	* @param ItemID The ID of the Item to split
	* @param StackSize The size of the created stack. Default = 1
	* @return true if the Split operation succeded, false otherwise
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory Management")
	bool SplitStackAt(uint8 SlotIdx, uint8 StackSize = 1);

	/** Reloads the Inventory GUI. Must be implemented using Blueprints. */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Inventory Management")
	void ReloadInventory();

protected:
	uint8 AddNewSlot(FInventorySlot &Slot);
};
