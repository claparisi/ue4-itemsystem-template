// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#pragma once

#include "GameFramework/Actor.h"
#include "Interactable.generated.h"

/**
* This class represents a visible Actor you can interact with.
*
* Should be used as a base class for all the in game items that react to a player's action.
* Interact != Using. The player interacts with an item that's visible in the game world.
* Examples are items you can pickup, lights that can be turned on, treasure chests that can be opened, etc.
*
* It should NOT be used as a base class for NPCs the player can interact with.
* Those should inherit from a more appropriate subclass of Pawn.
*/
UCLASS()
class NEVLD_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** The name of this Interactable */
	UPROPERTY(EditDefaultsOnly)
	FString Name;
	
	/** The hint or the interaction type to display when facing this Interactable */
	UPROPERTY(EditDefaultsOnly)
	FString Hint;

	/** The Mesh associated to this Interactable */
	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* InteractableMesh;

public:

	/** Called when we interact with this Interactable.
	* A Designer can choose to implement it using both blueprints or C++ (overriding the _Implementation)
	*/
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Functions")
	void Interact(ANevLDPlayerController* Controller);
	virtual void Interact_Implementation(ANevLDPlayerController* Controller);

	/** Gets a hint about what this Interactable does
	* @return An FString of the form 'Interactable.Hint': 'Interactable.Name' 
	*/
	UFUNCTION(BlueprintCallable, Category = "Interact Functions")
	FString GetInteractionHint() const { 
		return FString::Printf(TEXT("%s: %s"), *Hint, *Name); 
	}
};
