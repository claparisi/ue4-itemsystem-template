// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#include "NevLD.h"
#include "NevLDGameInstance.h"


struct FStorableRow* UNevLDGameInstance::GetStorableFromDataTable(FName ItemID) {
	if(StorableDataTable != nullptr)
		return StorableDataTable->FindRow<FStorableRow>(ItemID, "");
	return nullptr;
}