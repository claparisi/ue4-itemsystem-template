// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#include "NevLD.h"
#include "NevLDPlayerController.h"
#include "NevLDCharacter.h"
#include "ItemSystem/Storable.h"
#include "Engine.h"


ANevLDPlayerController::ANevLDPlayerController() {
	bAllowTickBeforeBeginPlay = false;
	InventoryClass = UInventoryComponent::StaticClass();
}

void ANevLDPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();
	InputComponent->BindAction("Interact", IE_Pressed, this, &ANevLDPlayerController::Interact);
	InputComponent->BindAction("Inventory", IE_Pressed, this, &ANevLDPlayerController::ToggleInventory);
}



void ANevLDPlayerController::BeginPlay()
{
	Super::BeginPlay();
	if (InventoryClass == NULL) {
		UE_LOG(LogTemp, Error, TEXT("PlayerController: No InventoryClass has been defined. Unable to initialize the InventoryComponent."));
		return;
	}
	InventoryComponent = NewObject<UInventoryComponent>(this, InventoryClass, "InventoryComponent");
	InventoryComponent->RegisterComponent();
}

void ANevLDPlayerController::Interact()
{
	if (InSightInteractable != nullptr)
		InSightInteractable->Interact(this);
}