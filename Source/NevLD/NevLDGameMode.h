// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "NevLDGameMode.generated.h"

UCLASS(minimalapi)
class ANevLDGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ANevLDGameMode();
};



